const showLogs = false;
function logDebug(s) {
  if (showLogs) console.log(s);
}

module.exports = function (babel) {
  const { types: t } = babel;
  const locSep = ',';
  let readPropId = null;
  return {
    visitor: {
      Program: {
        enter(path, state) {
          const readPropFuncStr = `
          (function () {
            var thisFileName = "` + state.file.opts.filename + `";
            var throwNoPropException = function (o, t, p, locStr) {
              const locs = locStr['split']('` + locSep + `');
              const l0 = locs[0];
              const c0 = locs[1];
              const l1 = locs[2];
              const c1 = locs[3];
              const prettyLocStr = "line " + (l0 === l1 ? l0 : l0 + "-" + l1) + ", col " + (c0 === c1 ? c0 : c0 + "-" + c1);
              const msg = thisFileName + ", " + prettyLocStr + ": " + (o === null ? "null" : t) + " has no property '" + p + "'";
              throw new ReferenceError(msg, thisFileName, l0);//{name: "StrictPropertiesException", message: msg};
            };
            return function (o, p, locStr) {
              const ot = typeof o;
              if (((ot === 'boolean' || ot === 'undefined' || ot === 'number' || ot === 'symbol' || ot === 'string') && o[p] === undefined) ||
                  (ot === 'object' && (o === null || !(p in o)))) {
                throwNoPropException(o, ot, p, locStr);
              } else {
                const m = o[p];
                const mt = typeof m;
                return mt === 'function' ? m['bind'](o) : m;
              }
            }
          })()
          `;
          readPropId = path.scope.generateUidIdentifier('readProp');
          const newNode = babel.transform("var " + readPropId.name + " = " + readPropFuncStr).ast.program.body;
          path.unshiftContainer("body", newNode);
        }
      },
      MemberExpression(path) {
        const n = path.node;
        if (n.computed || !n.loc) return;
        const locStr = n.loc.start.line + locSep + (n.loc.start.column + 1) + locSep + n.loc.end.line + locSep + (n.loc.end.column + 1);
        const newNode = t.callExpression(readPropId, [n.object, t.stringLiteral(n.property.name), t.stringLiteral(locStr)]);

        try {
          //t.NODE_FIELDS[path.parent.type][path.parentKey].validate(path.parent, path.parentKey, newNode);
          path.replaceWith(newNode);
          logDebug("transforming " + path.parent.type);
        } catch (e) {
          if (e.name === 'TypeError') {
            logDebug("skipping " + path.parent.type + " " + path.parentKey + ": " + e.message);
            return;
          } else {
            throw e;
          }
        }
      }
    }
  };
}
